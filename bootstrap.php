<?php 

require_once(__DIR__ . '/vendor/autoload.php');

use FastRoute\RouteCollector;

use FastRoute\Dispatcher;

class app {

private $injector;

private $request;

private $response;

function __construct () {

$this->injector = require(__DIR__ . '/config.php');

$this->request = $injector->make('Http\HttpRequest');

$this->response = $injector->make('Http\HttpResponse');


}

public function run () {


$dispatcher = FastRoute\simpleDispatcher(function(RouteCollector $rc) {

    $rc->get('/', ['Test\Http\Controllers\IndexController', 'index']);

    $rc->addGroup('/api', function (RouteCollector $rc) {

        $rc->get('/convert/{from}/{to}/{amount}', ['Test\Http\Controllers\Api\ConverterController', 'index']);
    });

});


$route = $dispatcher->dispatch($this->request->getMethod(), $this->request->getPath());

switch ($route[0]) {

    case Dispatcher::NOT_FOUND:

        $response->setContent('Page not found');
        $response->setStatusCode(404);
    
        break;

    case Dispatcher::METHOD_NOT_ALLOWED:
    
        $response->setContent('Method not allowed');
        $response->setStatusCode(405);
    
        break;

    case Dispatcher::FOUND:

        $this->injector->make($route[1][0])->{$route[1][1]}($route[2]);

        break;
}

foreach ($this->response->getHeaders() as $header) header($header);

echo $this->response->getContent();

}


function __destruct () {

unset($this->injector);

unset($this->request);

unset($this->response);

}

}