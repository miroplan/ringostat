<?php

namespace Test\Http\Controllers;

use Test\Interfaces\CurrenciesServiceInterface;
use Test\Interfaces\RendererInterface;
use Http\Response;

class IndexController
{
    /** @var Response */
    private $response;

    /** @var RendererInterface */
    private $renderer;

    /** @var CurrenciesServiceInterface */
    private $service;

    /**
     * IndexController constructor.
     * @param Response $response
     * @param RendererInterface $renderer
     * @param CurrenciesServiceInterface $service
     */
    public function __construct(
        Response $response,
        RendererInterface $renderer,
        CurrenciesServiceInterface $service
    )
    {
        $this->response = $response;
        $this->renderer = $renderer;
        $this->service = $service;
    }

    /**
     * Set response to frontend: index.html with currencies list
     */
    public function index()
    {
        $html = $this->renderer->render('index.html', [
            'currencies' => $this->service->getCurrencies()
        ]);
        $this->response->setContent($html);
    }
}